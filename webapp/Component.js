sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"turktraktor/zcrm/sikayet/model/models"
], function(UIComponent, Device, models) {
	"use strict";
	return UIComponent.extend("turktraktor.zcrm.sikayet.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			try {

				var objShell = sap.ui.getCore().byId("shell");

				objShell.setHeaderHiding(false);

				objShell.setHeaderHidingDelay(0);

			} catch (notInFioriLaunchPad) {}

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			// set the master model for master page
			this.setModel(models.createJSONModel(), "master");
			// set the detail model for sikayetdetail page
			this.setModel(models.createJSONModel(), "detail");
			// set the update json model for update service
			this.setModel(models.createJSONModel(), "update");

			this.getRouter().initialize();
		},
		getContentDensityClass: function() {
			if (this._sContentDensityClass === undefined) {
				// check whether FLP has already set the content density class; do nothing in this case
				if (jQuery(document.body).hasClass("sapUiSizeCozy") || jQuery(document.body).hasClass("sapUiSizeCompact")) {
					this._sContentDensityClass = "";
				} else {
					// Store "sapUiSizeCompact" or "sapUiSizeCozy" in this._sContentDensityClass, depending on which modes are supported by the app.
					// E.g. the “cozy” class in case sap.ui.Device.support.touch is “true” and “compact” otherwise.
					this._sContentDensityClass = "sapUiSizeCompact";
				}
			}
			return this._sContentDensityClass;
		}
	});
});