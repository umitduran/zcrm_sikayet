sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function() {
			var oDeviceModel = new JSONModel(Device);
			oDeviceModel.setDefaultBindingMode("OneWay");
			return oDeviceModel;
		},
		createJSONModel : function () {
			var oModel = new JSONModel();
			oModel.setDefaultBindingMode("TwoWay");
			return oModel;
		}

	};
});