sap.ui.define([], function() {
	"use strict";
	return {
		statusText: function(sStatus) {
			switch (sStatus) {
				case "E0019":
					return "Success";
				case "E0009":
					return "Error";
			}
		},
		yedekParcaTeslimTarihi: function(sDate) {
			// sap.ui.model.type.Date
			if (sDate && sDate.length > 0) {
				var aDateTimestamp = sDate.split("(")[1].split(")")[0];
				var date = new Date(aDateTimestamp / 1);
				return date.toLocaleDateString();
			}
			return sDate;
		},
		attachmentVisibility: function(aAttachments) {
			if (aAttachments && aAttachments.length > 0) {
				return true;
			}
			return false;
		}
	};
});