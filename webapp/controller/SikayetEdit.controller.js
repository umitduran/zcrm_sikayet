sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	'sap/m/MessageToast'
], function(Controller, Filter, FilterOperator, MessageToast) {
	"use strict";

	return Controller.extend("turktraktor.zcrm.sikayet.controller.SikayetEdit", {

		onInit: function() {
			var oView = this.getView();
			var oComp = this.getOwnerComponent();
			oView.addStyleClass(oComp.getContentDensityClass());
			this.getRouter().attachRoutePatternMatched(this._onRouteMatched, this);
		},
		getRouter: function() {
			var oComponent = this.getOwnerComponent();
			return oComponent.getRouter();
		},

		_onRouteMatched: function(oEvent) {
			var sRouteName = oEvent.getParameter("name");
			if (sRouteName === "sikayetedit") {
				var oDetailModel = this.getView().getModel("detail");
				oDetailModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
				// oDetailModel.setProperty("/Textid", "");
				// oDetailModel.setProperty("")

				this._onGetComplaintStatuList(this, oEvent);
				this._onFillUpdateYedekParcaList(this);
				this._setNotLengthToModel();
				this._resetUnupdatedValues();
				this._fillComplaintNoteTypes();
				var oDetailModel = this.getView().getModel("detail");
				this._oldStatus = oDetailModel.getProperty("/Status");
				oDetailModel.setProperty("/SelectedComplaintNoteType", "");
				oDetailModel.setProperty("/GirilenNot", "");
			}
		},

		_resetUnupdatedValues: function() {
			var oView = this.getView();
			var oDetailModel = oView.getModel("detail");
			var oNotTextArea = this.byId("idTextNotAlani");
			var oStatusComboBox = this.byId("idComboBoxDurum");
			var sOldNotValue = oDetailModel.getProperty("/NotAlani");
			var sOldStatusKey = oDetailModel.getProperty("/Status");
			oNotTextArea.setValue(sOldNotValue);
			oStatusComboBox.setSelectedKey(sOldStatusKey);
		},

		_fillComplaintNoteTypes: function() {
			var oDataModel = this.getView().getModel();
			var oDetailModel = this.getView().getModel("detail");
			oDataModel.read("/ComplaintNoteTypeSet", {
				success: function(resp) {
					oDetailModel.setProperty("/ComplaintNoteTypes", resp.results);
					if (resp.results && resp.results.length > 0) {
						oDetailModel.setProperty("/ComplaintNoteType", "ZS07");
					}
				},
				error: function(err) {

				}
			});
		},

		_setNotLengthToModel: function() {
			var oDetailModel = this.getView().getModel("detail");
			var sNot = oDetailModel.getProperty("/NotAlani");
			var iNotLength = sNot.length;
			oDetailModel.setProperty("/NotKarakterSayisi", iNotLength);
		},
		_onGetComplaintStatuList: function(oController, oEvent) {
			var oArguments = oEvent.getParameter("arguments");
			var sObjectGuid = oArguments.objectguid;
			var oDataModel = oController.getView().getModel();
			var oDetailModel = oController.getView().getModel("detail");
			var sObjectGuid = oDetailModel.getProperty("/ObjectGuid");

			this._resetModifiedSources();

			var self = this;

			oDataModel.read("/ComplaintStatuListSet", {
				filters: [
					new Filter({
						path: 'ObjectGuid',
						operator: FilterOperator.EQ,
						value1: sObjectGuid
					}),
					new Filter({
						path: 'Langu',
						operator: FilterOperator.EQ,
						value1: 'T'
					})
				],
				success: function(resp) {
					var aStatuList = [];
					jQuery.each(resp.results, function(key, el) {
						var row = {
							Status: el.Status,
							StatusText: el.StatusText
						}
						aStatuList.push(row);
					});
					oDetailModel.setProperty("/StatusList", aStatuList);
				},
				error: function(err) {
					var sErrorMessage = self.getBundleText("textGetComplaintListError");
					MessageToast.show(sErrorMessage);
				}
			});
		},

		_onFillUpdateYedekParcaList: function(oController) {
			var oDetailModel = oController.getView().getModel("detail");
			var oData = oDetailModel.getData();
			var oUpdateModel = {};
			oUpdateModel.PRODUCT_ID_TAB = [];

			jQuery.each(oData.ComplaintToProductList, function(key, el) {
				var rowProductIdTab = {
					ProductId: el.ProductId,
					RefGuid: el.RefGuid
				}
				oUpdateModel.PRODUCT_ID_TAB.push(rowProductIdTab);
			});

			oDetailModel.updatedModel = oUpdateModel;

		},

		getBundleText: function(sKey, sParameter1, sParameter2, sParameter3, sParameter4) {
			var i18nModel = this.getView().getModel("i18n");
			var oBundle = i18nModel.getResourceBundle();
			var sValue = oBundle.getText(sKey, [sParameter1, sParameter2, sParameter3, sParameter4]);
			return sValue;
		},

		onBackSikayetDetail: function() {
			var sEditModel = this.getView().getModel("detail");
			var sObjectGuid = sEditModel.getProperty("/ObjectGuid");

			this._resetModifiedSources();

			// this.getRouter().navTo("sikayetdisplay", {
			// 	objectguid: sObjectGuid
			// });
			window.history.back();
		},
		onSikayetCancel: function(oEvent) {
			var sEditModel = this.getView().getModel("detail");
			var sObjectGuid = sEditModel.getProperty("/ObjectGuid");

			this._resetModifiedSources();

			this.getRouter().navTo("sikayetdisplay", {
				objectguid: sObjectGuid
			});
		},

		onSikayetUpdate: function(oEvent) {
			var oDataModel = this.getView().getModel();
			var oDetailModel = this.getView().getModel("detail");
			var oData = oDetailModel.getData();
			var oView = this.getView();
			var oComboBoxStatu = oView.byId("idComboBoxDurum");
			var oDateTeslim = oView.byId("idDatePickerTeslim");
			var oInputNotAlani = oView.byId("idTextNotAlani");
			var sStatus = oComboBoxStatu.getSelectedKey();
			var sStatusText = oComboBoxStatu.getValue();
			//var oTeslimTarihiValue = oDateTeslim.getValue();
			var sNotAlani = oInputNotAlani.getValue();
			var sObjectGuid = oDetailModel.getProperty("/ObjectGuid");
			// idDPYedekParcaTeslimTarihi
			var oDateYPTeslim = oView.byId("idDPYedekParcaTeslimTarihi");
			var oDateYp = oDateYPTeslim.getDateValue();
			if (oDateYp) {
				oDateYp.setHours(15);
			}
			// Update Edilen Alanlar
			oDetailModel.oData.Status = sStatus;
			oDetailModel.oData.StatusText = sStatusText;
			oDetailModel.oData.NotAlani = sNotAlani;

			var oUpdateModel = {};
			oUpdateModel.PRODUCT_ID_TAB = [];
			oUpdateModel.ObjectGuid = sObjectGuid;
			oUpdateModel.Status = sStatus;
			oUpdateModel.NotAlani = sNotAlani;
			oUpdateModel.YedekParcaDate = oDateYp;

			// oUpdateModel.NotAlani = this.byId("idGirilenNot").getValue();
			// oUpdateModel.NoteType = this.byId("idSelectedComplaintNoteType").getSelectedKey();

			oUpdateModel.NoteType = oData.ComplaintNoteType;
			oUpdateModel.NotAlani = oData.NoteTextDialog;

			jQuery.each(oDetailModel.updatedModel.PRODUCT_ID_TAB, function(key, el) {
				if (el) {
					var rowProductIdTab = {
						ProductId: el.ProductId,
						RefGuid: el.RefGuid
					}
				}
				oUpdateModel.PRODUCT_ID_TAB.push(rowProductIdTab);
			});

			/*			var oTestModel = {
							ObjectGuid : "005056A7-2A41-1ED7-8CFE-963C71DF00CD",
							Status : "E0009",
							NotAlani : "08.05.2017 17:49:32 BULENTB Parça new holland parçasıdır ve ",
							YedekParcaDate : new Date(),   //"20172908220000"
							PRODUCT_ID_TAB : [
								{
									RefGuid : "005056A7-2A41-1ED7-8CFE-963C71DF00CD",
									ProductId : "20291711",
									ProductText : "ProductText"
								}
							
							]
							
						}*/

			var sCategory = oData.Categori;
			var sCategory1 = oData.Categori1;
			var sCategory2 = oData.Categori2;
			var sCategory3 = oData.Categori3;
			var sCategory4 = oData.Categori4;

			if (sCategory && sCategory1 && sCategory2 && sCategory3 && sCategory4) {
				var self = this;
				MessageToast.show("Güncelleniyor");
				this.getView().setBusy(true);
				oDataModel.create('/ComplaintUpdateSet', oUpdateModel, {
					success: function(resp) {
						self.getView().setBusy(false);
						var sSuccessMessage = self.getBundleText("textSikayetUpdateSuccess");
						MessageToast.show(sSuccessMessage);

						self.onSikayetCancel();
					},
					error: function(err) {
						self.getView().setBusy(false);
						try {
							var sResponseText = err.responseText;
							var oResponseText = JSON.parse(sResponseText);
							var aErrorDetails = oResponseText.error.innererror.errordetails;
							if (aErrorDetails && aErrorDetails.length > 0) {
								var sMessage = "";
								for (var i = 0; i < aErrorDetails.length; i++) {
									sMessage += aErrorDetails[i].message + "\n";
								}
								sap.m.MessageBox.error(sMessage);
							}
							// JSON.parse(err.responseText).error.innererror.errordetails
						} catch (e) {

						}
						// var sSuccessMessage = self.getBundleText("textSikayetUpdateError");
						// MessageToast.show(sSuccessMessage);
					}
				});
			} else {
				MessageToast.show("Şikayetin kategorisi olmadığı için güncellenemiyor");
			}

		},

		onHandleTeslimTarihi: function(oEvent) {
			var oDatePicker = oEvent.getSource();
			var bValid = oEvent.getParameter("valid");
			if (bValid) {
				oDatePicker.setValueState(sap.ui.core.ValueState.None);
			} else {
				oDatePicker.setValueState(sap.ui.core.ValueState.Error);
			}
		},

		onYedekParcaValueHelp: function(oEvent) {
			this._oYedekParcaDialog = sap.ui.xmlfragment("turktraktor.zcrm.sikayet.fragments.YedekParca", this);
			this.getView().addDependent(this._oYedekParcaDialog);
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oYedekParcaDialog);
			this._oYedekParcaDialog.open();
		},

		onOpenNotEkleDialog: function(oEvent) {
			if (!this._oNotEkleDialog) {
				this._oNotEkleDialog = sap.ui.xmlfragment("turktraktor.zcrm.sikayet.fragments.NotEkleDialog", this);
				this.getView().addDependent(this._oNotEkleDialog);
				// jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oNotEkleDialog);
			}
			this._oNotEkleDialog.open();
		},

		onYedekParcaDialogSearch: function(oEvent) {
			var oController = this;
			var oParameters = oEvent.getParameters();
			var sValue1 = oParameters.value;
			var oDataModel = this.getView().getModel();
			if (sValue1.length >= 3) {
				oDataModel.read("/ComplaintProductListSet", {
					filters: [new sap.ui.model.Filter({
						path: 'ShortText',
						operator: sap.ui.model.FilterOperator.EQ,
						value1: sValue1
					})],
					urlParameters: {
						"$top": "100",
						"$skip": "0"
					},
					success: function(resp) {

						var aYedekParca = [];
						jQuery.each(resp.results, function(key, el) {
							var row = {
								ProductGuid: el.ProductGuid,
								ProductId: el.ProductId,
								ShortText: el.ShortText
							};
							aYedekParca.push(row);
						});
						var oUpdateModel = oController.getView().getModel("update");
						oUpdateModel.setProperty("/ComplaintProductListSet", aYedekParca);

					},
					error: function(err) {
						MessageToast.show("Ürünler getirilemedi!");
					}

				});
			}

		},

		onYedekParcaDialogCancel: function(oEvent) {
			var aContexts = oEvent.getParameter("selectedContexts");
			if (aContexts && aContexts.length) {
				MessageToast.show(" " + aContexts.map(function(oContext) {
					return oContext.getObject().Name;
				}).join(", "));
			}
			oEvent.getSource().getBinding("items").filter([]);
			var oUpdateModel = this.getView().getModel("update");
			oUpdateModel.setProperty("/ComplaintProductListSet", []);
		},

		onYedekParcaDialogClose: function(oEvent) {
			var oDetailModel = this.getView().getModel("detail");
			var aContexts = oEvent.getParameter("selectedContexts");
			var aComplaintToProductList = oDetailModel.getProperty("/ComplaintToProductList");
			if (aContexts && aContexts.length) {
				var sPath = aContexts[0].sPath;
				var oProductListModel = aContexts[0].oModel;
				var oSelectedProduct = oProductListModel.getProperty(sPath);
				aComplaintToProductList.push(oSelectedProduct);
				oDetailModel.setProperty("/ComplaintToProductList", aComplaintToProductList);
				oSelectedProduct.RefGuid = oSelectedProduct.ProductGuid;
				oDetailModel.updatedModel.PRODUCT_ID_TAB.push(oSelectedProduct);
				var oUpdateModel = this.getView().getModel("update");
				oUpdateModel.setProperty("/ComplaintProductListSet", []);
			}
		},

		onDeleteYedekParca: function(oEvent) {
			var oDetailModel = this.getView().getModel("detail");
			var oSource = oEvent.getSource();
			var oParent = oSource.getParent();
			var sBindingContextPath = oParent.getBindingContextPath();
			var sPath = sBindingContextPath.match(/\d/g).join("");
			delete oDetailModel.updatedModel.PRODUCT_ID_TAB[sPath];

			var oModifiedSources = oDetailModel.getProperty("/ModifiedSources");
			if (!oModifiedSources) {
				oModifiedSources = [];
			}
			oSource.setType("Reject");
			oModifiedSources.push(oSource);
			oDetailModel.setProperty("/ModifiedSources", oModifiedSources);
			//oDetailModel.updatedModel.PRODUCT_ID_TAB.splice(sPath, 1);
		},

		onComboBoxDurumChange: function(oEvent) {
			var oDetailModel = this.getView().getModel("detail");
			var oParameters = oEvent.getParameters();
			var oComboBox = this.byId(oParameters.id);

			var oYpDateInput = this.byId("idDPYedekParcaTeslimTarihi");
			if (oYpDateInput) {
				oYpDateInput.setValueState(sap.ui.core.ValueState.None);
			}

			var sNewStatus = oComboBox.getSelectedKey();
			var sHedefAlani = oDetailModel.getProperty("/HedefAlani");
			var sNot = oDetailModel.getProperty("/NoteTextDialog");
			var sMessage = "";
			if ((sNewStatus === "E0013" || sNewStatus === "E0015" || sNewStatus === "E0017") && !sNot) {
				oComboBox.setSelectedKey(this._oldStatus);
				sMessage += "Çözüm notu giriniz. \n";
			}
			if (sHedefAlani === "Z000000001") {
				var sYpDate = oDetailModel.getProperty("/YpDate");
				var aProducts = oDetailModel.getProperty("/ComplaintToProductList");
				var iProductCount = aProducts ? aProducts.length : 0;

				if ((sNewStatus === "E0013" || sNewStatus === "E0015") && !sYpDate) {
					oComboBox.setSelectedKey(this._oldStatus);
					sMessage += "Yedek parça teslim tarihi giriniz. \n";
					if (oYpDateInput) {
						oYpDateInput.setValueState(sap.ui.core.ValueState.Error);
					}
				}

				if ((sNewStatus === "E0013" || sNewStatus === "E0015") && iProductCount < 2) {
					oComboBox.setSelectedKey(this._oldStatus);
					sMessage += "Yedek parça bilgisi giriniz. \n";
				}
			}
			if (sMessage) {
				sap.m.MessageBox.error(sMessage);
			} else {
				this._oldStatus = sNewStatus;
			}

		},

		onYpDatePickerChange: function(oEvent) {
			var oYpDateInput = this.byId("idDPYedekParcaTeslimTarihi");
			if (oYpDateInput && oYpDateInput.getValueState() === "Error") {
				oYpDateInput.setValueState(sap.ui.core.ValueState.None);
			}
		},

		// onBayiChange: function(oEvent) {
		// 	var oidInputBayi = this.byId("idInputBayi");
		// 	if (oidInputBayi && oidInputBayi.getValueState() === "Error") {
		// 		oidInputBayi.setValueState(sap.ui.core.ValueState.None);
		// 	}
		// },

		onNotAlaniValueChange: function(oEvent) {
			var oDetailModel = this.getView().getModel("detail");
			var oParameters = oEvent.getParameters();
			var sNewNot = oParameters.newValue;
			var iNotLength = sNewNot.length;

			if (iNotLength && iNotLength > 1024) {
				var oNotTextArea = this.getView().byId(oParameters.id);
				var sNotLimited = sNewNot.substring(0, 1024);
				oNotTextArea.setValue(sNotLimited);
				iNotLength = sNotLimited.length;
			}

			oDetailModel.setProperty("/NotKarakterSayisi", iNotLength);
		},

		_openNoteEmptyWarningDialog: function() {
			if (!this.notDegistirilmediDialog) {
				this.notDegistirilmediDialog = sap.ui.xmlfragment("turktraktor.zcrm.sikayet.fragments.NotDegistirilmediDialog", this);
				this.getView().addDependent(this.notDegistirilmediDialog);
			}
			this.notDegistirilmediDialog.open();
		},

		closeNotDegistirilmedieDialog: function(oEvent) {
			if (this.notDegistirilmediDialog) {
				this.notDegistirilmediDialog.close();
			}
		},

		afterCloseNotDegistirilmedieDialog: function(oEvent) {
			if (this.notDegistirilmediDialog) {
				this.notDegistirilmediDialog.destroy();
			}
		},
		_resetModifiedSources: function() {
			var oDetailModel = this.getView().getModel("detail");
			var oModifiedSources = oDetailModel.getProperty("/ModifiedSources");
			if (oModifiedSources && oModifiedSources.length) {
				oModifiedSources.forEach(function(oModifiedSource) {
					oModifiedSource.setType("Transparent");
				});
			}
		},

		onNotEkleCancel: function(oEvent) {
			if (this._oNotEkleDialog) {
				this._oNotEkleDialog.close();
			}
		},

		onNotEkleOnayla: function(oEvent) {
			var oDetailModel = this.getView().getModel("detail");

			if (this._oNotEkleDialog) {
				this._oNotEkleDialog.close();
			}
		}

	});

});