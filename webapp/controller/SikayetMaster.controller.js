sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"turktraktor/zcrm/sikayet/model/formatter",
	'sap/m/MessageToast'
], function(Controller, Filter, FilterOperator, formatter, MessageToast) {
	"use strict";

	return Controller.extend("turktraktor.zcrm.sikayet.controller.SikayetMaster", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf hcm.empvisareq.view.Master
		 */
		formatter: formatter,
		_top: 100,
		_skip: 0,
		_isInitial: true,

		onInit: function() {
			var oView = this.getView();
			var oComp = this.getOwnerComponent();
			oView.addStyleClass(oComp.getContentDensityClass());
			this.getRouter().attachRoutePatternMatched(this._onRouteMatched, this);
		},
		getRouter: function() {
			var oComponent = this.getOwnerComponent();
			return oComponent.getRouter();
		},

		getBundleText: function(sKey, sParameter1, sParameter2, sParameter3, sParameter4) {
			var i18nModel = this.getView().getModel("i18n");
			var oBundle = i18nModel.getResourceBundle();
			var sValue = oBundle.getText(sKey, [sParameter1, sParameter2, sParameter3, sParameter4]);
			return sValue;
		},

		_onRouteMatched: function(oEvent) {
			var sRouteName = oEvent.getParameter("name");
			if (sRouteName === "sikayet") {
				this._loadSikayetMasterPage(this);
			}
		},
		
		_navBack: function(oEvent) {
			window.history.back();
		},
		
		_loadSikayetMasterPage: function(oController) {
			// oController.getView().setBusy(true);
			// var oDataModel = oController.getView().getModel();
			// var self = this;

			// oDataModel.read("/ComplaintSet", {
			// 	urlParameters: {
			// 		"$top": 30,
			// 		"$skip": 0
			// 	},
			// 	filters: [
			// 		new sap.ui.model.Filter({
			// 			path: "Status",
			// 			operator: sap.ui.model.FilterOperator.EQ,
			// 			value1: "E0019"
			// 		}),
			// 		new sap.ui.model.Filter({
			// 			path: "ProcessType",
			// 			operator: sap.ui.model.FilterOperator.EQ,
			// 			value1: "ZC11"
			// 		}),
			// 		new sap.ui.model.Filter({
			// 			path: "Description",
			// 			operator: sap.ui.model.FilterOperator.EQ,
			// 			value1: "3066"
			// 		})
			// 	],
			// 	success: function(resp) {
			// 		var aComplaints = [];
			// 		jQuery.each(resp.results, function(key, el) {
			// 			var row = {
			// 				Description: el.Description,
			// 				ObjectGuid: el.ObjectGuid,
			// 				ObjectId: el.ObjectId,
			// 				Status: el.Status,
			// 				StatusText: el.StatusText
			// 			};
			// 			aComplaints.push(row);
			// 		});
			// 		var oMasterModel = oController.getView().getModel("master");
			// 		oMasterModel.setProperty("/ComplaintSet", aComplaints);
			// 		oController.getView().setBusy(false);

			// 		self._getStatuTextList(self);

			// 		var sSuccessMessage = self.getBundleText("textLoadSikayetMasterSuccess");
			// 		MessageToast.show(sSuccessMessage);
			// 	},
			// 	error: function(err) {
			// 		oController.getView().setBusy(false);

			// 		var sSuccessMessage = self.getBundleText("textLoadSikayetMasterError");
			// 		MessageToast.show(sSuccessMessage);
			// 	}
			// });
			this._getComplaintSet(this, {
				isInitial: this._isInitial
			});
		},

		_getStatuTextList: function() {
			var oDataModel = this.getView().getModel();
			var oMasterModel = this.getView().getModel("master");
			var aComplaintSet = oMasterModel.getProperty("/ComplaintSet");
			if (aComplaintSet && aComplaintSet.length > 0) {
				var sObjectGuid = oMasterModel.getProperty("/ComplaintSet")[0].ObjectGuid;
				oDataModel.read("/ComplaintStatuListSet", {
					filters: [
						new Filter({
							path: 'ObjectGuid',
							operator: FilterOperator.EQ,
							value1: sObjectGuid
						}),
						new Filter({
							path: 'Langu',
							operator: FilterOperator.EQ,
							value1: 'T'
						})
					],
					success: function(resp) {
						var aStatuList = [
							// 	{
							// 	Status: '',
							// 	StatusText: ''
							// }
						];
						jQuery.each(resp.results, function(key, el) {
							var row = {
								Status: el.Status,
								StatusText: el.StatusText
							};
							aStatuList.push(row);
						});
						oMasterModel.setProperty("/StatusList", aStatuList);
					},
					error: function(err) {
						var sErrorMessage = self.getBundleText("textGetComplaintListError");
						MessageToast.show(sErrorMessage);
					}
				});
			}
		},

		onSelectSikayetDetail: function(oEvent) {
			var oSource = oEvent.getSource();
			var oContext = oSource.getBindingContext("master");
			var sPath = oContext.sPath;
			sPath = sPath.match(/\d/g).join("");
			var sObjectGuid = oContext.getProperty("/ComplaintSet")[sPath].ObjectGuid;
			this.getRouter().navTo("sikayetdisplay", {
				objectguid: sObjectGuid
			});
		},
		onSikayetSearch: function(oEvent) {
			var oMasterModel = this.getView().getModel("master");
			var aFilter = [];
			var sQuery = oEvent.getSource().getValue();

			// if (sQuery) {
			// 	var oDescriptionFilter = new Filter("Description", FilterOperator.Contains, sQuery);
			// 	var oObjectIdFilter = new Filter("ObjectId", FilterOperator.Contains, sQuery);
			// 	//var oStatusText = new Filter("StatusText", FilterOperator.Contains,sQuery);
			// 	var oFilterQuery = new Filter({
			// 		filters: [oDescriptionFilter, oObjectIdFilter],
			// 		and: false
			// 	});
			// 	aFilter.push(oFilterQuery);
			// }

			oMasterModel.setProperty("/QueryStr", sQuery);

			// this._getComplaintSet(this, {
			// 	Description: sQuery
			// });

			var sStatusTextQuery = oMasterModel.getProperty("/StatusTextQuery");

			// if (sStatusTextQuery && sStatusTextQuery.length > 0) {
			// 	var oStatusTextFilter = new Filter("StatusText", FilterOperator.Contains, sStatusTextQuery);
			// 	aFilter.push(oStatusTextFilter);
			// }
			// var oList = this.getView().byId("idSikayetList");
			// var oBinding = oList.getBinding("items");
			// var oFilter = new Filter({
			// 	filters: aFilter,
			// 	and: true
			// });
			// oBinding.filter(oFilter);

			// oMasterModel.setProperty("/ComplaintSet", []);

			this._skip = 0;
			this._isFilterChanged = true;

			this._getComplaintSet(this, {
				Description: sQuery,
				Status: sStatusTextQuery
			});

			// this.handleOpenDialogSearchWordsStartWith();

		},
		onSikayetRefresh: function(oEvent) {
			var oRefreshButton = oEvent.getParameter("refreshButtonPressed");
			if (oRefreshButton) {
				this._loadSikayetMasterPage(this);
			}

		},

		onDurumComboBoxSelect: function(oEvent) {
			//oEvent.getParameter("value")
			var aFilter = [];
			var oMasterModel = this.getView().getModel("master");
			var sQueryStr = oMasterModel.getProperty("/QueryStr");
			// if (sQueryStr) {
			// 	var oDescriptionFilter = new Filter("Description", FilterOperator.Contains, sQueryStr);
			// 	var oObjectIdFilter = new Filter("ObjectId", FilterOperator.Contains, sQueryStr);
			// 	//var oStatusText = new Filter("StatusText", FilterOperator.Contains,sQuery);
			// 	var oFilterQuery = new Filter({
			// 		filters: [oDescriptionFilter, oObjectIdFilter],
			// 		and: false
			// 	});
			// 	aFilter.push(oFilterQuery);
			// }

			var sSelectedDurum = oEvent.getSource().getSelectedKey();
			// if (sSelectedDurum) {
			// 	var oStatusTextFilter = new Filter("StatusText", FilterOperator.Contains, sSelectedDurum);
			// 	aFilter.push(oStatusTextFilter);
			// }

			// oMasterModel.setProperty("/ComplaintSet", []);
			this._skip = 0;
			this._isFilterChanged = true;
			oMasterModel.setProperty("/StatusTextQuery", sSelectedDurum);

			// var oList = this.getView().byId("idSikayetList");
			// var oBinding = oList.getBinding("items");
			// var oFilter = new Filter({
			// 	filters: aFilter,
			// 	and: true
			// });
			// oBinding.filter(oFilter);
			//oBinding.filter(aFilter);

			this._getComplaintSet(this, {
				Description: sQueryStr,
				Status: sSelectedDurum
			});
		},

		_getComplaintSet: function(oController, oParams) {
			var sProcessType = "ZC11";

			var aFilters = [];
			var oProcessTypeFilter = new sap.ui.model.Filter({
				path: "ProcessType",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: sProcessType
			});
			aFilters.push(oProcessTypeFilter);
			if (oParams) {
				if (oParams.Status && oParams.Status.length > 0) {
					var aStatusFilters = [];
					for (var i = 0; i < oParams.Status.length; i++) {
						var oStatusFilter = new sap.ui.model.Filter({
							path: "Status",
							operator: sap.ui.model.FilterOperator.EQ,
							value1: oParams.Status[i]
						});
						aStatusFilters.push(oStatusFilter);
					}
					var oStatusFilter2 = new sap.ui.model.Filter({
						filters: aStatusFilters,
						and: false
					});
					aFilters.push(oStatusFilter2);
				}
				if (oParams.Description) {
					var oDescriptionFilter = new sap.ui.model.Filter({
						path: "Description",
						operator: sap.ui.model.FilterOperator.EQ,
						value1: oParams.Description
					});
					aFilters.push(oDescriptionFilter);
				}
			}

			var oDataModel = oController.getView().getModel();
			var self = this;

			oController.getView().setBusy(true);
			oDataModel.read("/ComplaintSet", {
				urlParameters: {
					"$top": this._top,
					"$skip": this._skip
				},
				filters: aFilters,
				success: function(resp) {
					oController.getView().setBusy(false);
					oController._getComplaints = false;
					var aComplaints = [];
					jQuery.each(resp.results, function(key, el) {
						var row = {
							Description: el.Description,
							ObjectGuid: el.ObjectGuid,
							ObjectId: el.ObjectId,
							Status: el.Status,
							StatusText: el.StatusText
						};
						aComplaints.push(row);
					});
					var oMasterModel = oController.getView().getModel("master");
					var aComplaintsOld = oMasterModel.getProperty("/ComplaintSet");
					if (!oController._isFilterChanged && aComplaintsOld) {
						for (var i = 0; i < aComplaints.length; i++) {
							aComplaintsOld.push(aComplaints[i]);
						}
						// oMasterModel.setSizeLimit(aComplaintsOld.length);
						oMasterModel.setProperty("/ComplaintSet", aComplaintsOld);
					} else {
						// oMasterModel.setSizeLimit(aComplaints.length);
						oMasterModel.setProperty("/ComplaintSet", aComplaints);
					}
					oController._isFilterChanged = false;

					self._getStatuTextList(self);

					var sSuccessMessage = self.getBundleText("textLoadSikayetMasterSuccess");
					MessageToast.show(sSuccessMessage);

					if (!sap.ui.Device.system.phone && oParams.isInitial) {
						if (resp.results && resp.results[0]) {
							var sObjectGuid = resp.results[0].ObjectGuid;
							oController.getRouter().navTo("sikayetdisplay", {
								objectguid: sObjectGuid
							});
						}
						oController._isInitial = false;
					}
				},
				error: function(err) {
					oController.getView().setBusy(false);
					oController._getComplaints = false;

					var sSuccessMessage = self.getBundleText("textLoadSikayetMasterError");
					MessageToast.show(sSuccessMessage);
				}
			});
		},

		handleOpenDialogSearchWordsStartWith: function() {
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("turktraktor.zcrm.sikayet.fragments.filterMaster", this);
				this.getView().addDependent(this._oDialog);
				this.getView().getModel("master").setProperty("/QueryText", "");
			}
			this._oDialog
				.setFilterSearchCallback(null)
				.setFilterSearchOperator(sap.m.StringFilterOperator.AnyWordStartsWith)
				.open();

		},

		handleConfirm: function(oEvent) {

		},

		handleResetFilters: function(oEvent) {

		},

		_getComplaints: false,

		_processComplaintSetResponse: function(oController, resp) {

		},

		onComplaintListUpdate: function(oEvent) {
			// var oMasterModel = this.getView().getModel("master");
			// var sSelectedDurum = oMasterModel.getProperty("/StatusTextQuery");
			// var sQueryStr = oMasterModel.getProperty("/QueryStr");

			// if (this._getComplaints) {
			// 	this._skip = this._skip + this._top;
			// 	// this._getComplaints = false;
			// 	this._getComplaintSet(this, {
			// 		Description: sQueryStr,
			// 		Status: sSelectedDurum
			// 	});
			// }

		},

		onComplaintListUpdateFinished: function(oEvent) {
			// this._getComplaints = true;
		},
		handleSelectionFinish: function(oEvent) {
			var oMasterModel = this.getView().getModel("master");
			var sQueryStr = oMasterModel.getProperty("/QueryStr");

			var aSelectedItems = oEvent.getParameter("selectedItems");
			var aSelectedStatuses = [];
			for (var i = 0; i < aSelectedItems.length; i++) {
				aSelectedStatuses.push(aSelectedItems[i].getKey());
			}

			this._isFilterChanged = true;
			oMasterModel.setProperty("/StatusTextQuery", aSelectedStatuses);

			this._getComplaintSet(this, {
				Description: sQueryStr,
				Status: aSelectedStatuses
			});
		},
		handleSelectionChange: function(oEvent) {

		}

	});

});