/*eslint linebreak-style: ["error", "unix"]*/
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"turktraktor/zcrm/sikayet/model/formatter"
], function(Controller, Filter, FilterOperator, formatter) {
	"use strict";

	return Controller.extend("turktraktor.zcrm.sikayet.controller.SikayetDisplay", {

		formatter: formatter,

		onInit: function() {
			var oView = this.getView();
			var oComp = this.getOwnerComponent();
			oView.addStyleClass(oComp.getContentDensityClass());
			this.getRouter().attachRoutePatternMatched(this._onRouteMatched, this);
		},
		getRouter: function() {
			var oComponent = this.getOwnerComponent();
			return oComponent.getRouter();
		},

		_onRouteMatched: function(oEvent) {
			var sRouteName = oEvent.getParameter("name");
			if (sRouteName === "sikayetdisplay") {
				var sObjectGuid = oEvent.getParameter("arguments").objectguid;
				this._loadSikayetDetailPage(this, sObjectGuid);
				this._getAttachments(this, sObjectGuid);
			}
		},
		_getAttachments: function(oController, ObjectGuid) {
			// var oView = oController.getView();
			// var oDataModel = oView.getModel();
			// var oDetailModel = oView.getModel("detail");

			// oDataModel.read("/AttachmentSet", {
			// 	filters: [new Filter({
			// 		path: 'HeaderGuid',
			// 		operator: FilterOperator.EQ,
			// 		value1: ObjectGuid
			// 	})],
			// 	success: function(resp) {
			// 	},
			// 	error: function(err) {
			// 	}

			// });
		},

		_loadSikayetDetailPage: function(oController, ObjectGuid) {
			oController.getView().setBusy(true);
			var oView = oController.getView();
			var oDataModel = oView.getModel();
			var oDetailModel = oView.getModel("detail");
			//var sPath = '/ComplaintDetailSet(guid\''+ObjectGuid+'\')';
			oDetailModel.setData({});
			oDataModel.read("/ComplaintDetailSet", {
				filters: [new Filter({
					path: 'Guid',
					operator: FilterOperator.EQ,
					value1: ObjectGuid
				})],
				urlParameters: {
					"$expand": "ATTACHMENT,PRODUCT_ID_TAB,NOTES"
				},
				success: function(oData, resp) {
					oDetailModel.setProperty("/AltHedef", oData.results[0].AltHedef);
					oDetailModel.setProperty("/AltHedefText", oData.results[0].AltHedefText);
					oDetailModel.setProperty("/Bayi", oData.results[0].Bayi);
					oDetailModel.setProperty("/Categori", oData.results[0].Category);
					oDetailModel.setProperty("/Categori1", oData.results[0].Category1);
					oDetailModel.setProperty("/Categori2", oData.results[0].Category2);
					oDetailModel.setProperty("/Categori3", oData.results[0].Category3);
					oDetailModel.setProperty("/Categori4", oData.results[0].Category4);
					oDetailModel.setProperty("/CategoriText", oData.results[0].CategoryText);
					oDetailModel.setProperty("/Description", oData.results[0].Description);
					oDetailModel.setProperty("/HedefAlani", oData.results[0].HedefAlani);
					oDetailModel.setProperty("/HedefAlaniText", oData.results[0].HedefAlaniText);
					oDetailModel.setProperty("/Musteri", oData.results[0].Musteri);
					oDetailModel.setProperty("/MusteriFullName", oData.results[0].MusteriFullName);
					// var sNot = oData.results[0].NotAlani;
					// sNot = sNot.replace("____________________", "\n");
					// oDetailModel.setProperty("/NotAlani", sNot);
					oDetailModel.setProperty("/ObjectId", oData.results[0].ObjectId);
					oDetailModel.setProperty("/Priority", oData.results[0].Priority);
					oDetailModel.setProperty("/PriorityText", oData.results[0].PriorityText);
					oDetailModel.setProperty("/SasiNo", oData.results[0].SasiNo);
					oDetailModel.setProperty("/SorumluCalisan", oData.results[0].SorumluCalisan);
					oDetailModel.setProperty("/SorumluFullName", oData.results[0].SorumluFullName);
					oDetailModel.setProperty("/SorumluIl", oData.results[0].SorumluIl);
					oDetailModel.setProperty("/Status", oData.results[0].Status);
					oDetailModel.setProperty("/StatusText", oData.results[0].StatusText);
					oDetailModel.setProperty("/ObjectGuid", oData.results[0].Guid);
					oDetailModel.setProperty("/YpDate", oData.results[0].YpDate);
					var oItems = oData.results[0].PRODUCT_ID_TAB.results;
					var aProductIdTabs = [];
					jQuery.each(oItems, function(key, el) {
						var row = {
							ProductId: el.ProductId,
							RefGuid: el.RefGuid,
							ShortText: el.ShortText
						}
						aProductIdTabs.push(row);
					});
					if (aProductIdTabs && aProductIdTabs.length > 0) {
						oDetailModel.setProperty("/Ekipman", aProductIdTabs[0]);
					}

					oDetailModel.setProperty("/ComplaintToProductList", aProductIdTabs);
					var aAttachments = oData.results[0].ATTACHMENT.results;
					oDetailModel.setProperty("/AttachmentList", aAttachments);

					var sYedekParcaTabCount = aProductIdTabs.length.toString();
					var oYedekParcaTab = oController.byId("idYedekParcaListeTab");
					oYedekParcaTab.setCount(sYedekParcaTabCount);

					oController.getView().setBusy(false);

					var aNotes = oData.results[0].NOTES.results;
					if (aNotes) {
						var sNotes = "";
						jQuery.each(aNotes, function(key, el) {
							var sLine = "";
							var sText = "-------------------------\n" + el.Etag + " " + el.Creator + "\n" + el.Content.replace("↵", "\n") + "\n\n";
							sNotes += sText;
							// aProductIdTabs.push(row);
						});
						oDetailModel.setProperty("/NotAlani", sNotes);
					}
				},
				error: function(err) {
					oController.getView().setBusy(false);
				}

			});
		},
		onSikayetEdit: function() {
			var oDetailModel = this.getView().getModel("detail");
			var sObjectGuid = oDetailModel.getProperty("/ObjectGuid");
			this.getRouter().navTo("sikayetedit", {
				objectguid: sObjectGuid
			});
		},
		onBackSikayetMaster: function() {
			this.getRouter().navTo("sikayet");
		},
		downloadURL: function(url) {
			var $idown;
			if ($idown) {
				$idown.attr('src', url);
			} else {
				$idown = $('<iframe>', {
					id: 'idown',
					src: url
				}).hide().appendTo('body');
			}
		},

		onDownloadButton: function(oEvent) {
			var sPath = oEvent.getSource().getParent().oBindingContexts.detail.sPath;
			var oDetailModel = this.getView().getModel("detail");
			var oAttachment = oDetailModel.getProperty(sPath);
			var url = oAttachment.__metadata.media_src;

			var url = url.substring(url.indexOf("/sap/opu"));
			this.downloadURL(url);
		}

	});

});